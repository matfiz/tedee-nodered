# Tedee Nodered MQTT

NodeRed flow to integrate Gerda Tedee lock with MQTT. It subscribes to MQTT topic `tedee/#` and pushes result to the same topic (it can be changed in the flow). 

## Getting started

Assuming you already have a working [NodeRed](https://nodered.org/) installation:
* install the required libraries in the Palette: `node-red-contrib-credentials`
* go to Menu->Import and copy the flow from the file in this repo.

The flow is based on Tedee API 1.25 on the [Personal Key authentication](https://tedee-tedee-api-doc.readthedocs-hosted.com/en/latest/howtos/authenticate.html). In order to make the flow operational, you need to peform the following steps:
1. Go to [Tedee portal](https://portal.tedee.com) and generate a personal key
2. Insert the key to *Tedee credentials* block along with your lock_id.
3. Setup MQTT server connection params accoring to your local setup. 

## Operations

### Locking/unlocking
Just push the message `locked` or `unlocked` to the topic `tedee/set/lock`. In case of a successful operation, the flow will respond by pushing a message `locked` or `unlocked` to `tedee/lock` topic.

### Lock status
Every 5 minutes, the flow will push a JSON message with lock status to `tedee/status` with the following information:
```json
{
    "id":9999,
    "isConnected":true,
    "lockProperties":{
        "state":2,
        "isCharging":false,
        "batteryLevel":23,
        "stateChangeResult":0,
        "lastStateChangedDate":"2022-01-27T08:55:07.362"
        }
    }
```

## Integrate with your tools

The MQTT flow integrates easily with smart home systems like OpenHab. 

### OpenHab integration

#### Things
```
Thing mqtt:topic:tedee-front "Lock front" (mqtt:broker:mosquitto) @ "gGroundFloor" {
Channels:
    Type switch : lock     "Lock"                [ stateTopic="tedee/lock", commandTopic="tedee/set/lock",on="locked", off="unlocked" ]
    Type number : battery "Battery" [ stateTopic="tedee/status", transformationPattern="JSONPATH:$.lockProperties.batteryLevel"]
    Type string   : last_state_change "Last state changed" [ stateTopic="tedee/status", transformationPattern="JSONPATH:$.lockProperties.lastStateChangedDate"]
    Type number   : state "State" [ stateTopic="tedee/status", transformationPattern="JSONPATH:$.lockProperties.state"]
}
```

#### Items
```
Switch tedeeLockFront "Front door [%s]" <button> (gFrontDoorLock) ["Switch"] { channel="mqtt:topic:tedee-front:lock", profile="transform:MAP", function="tedee" }
Switch tedeeLockFrontStatus "Front door - status [%s]" <button> (gFrontDoorLock) { channel="mqtt:topic:tedee-front:state", profile="transform:MAP", function="tedee" }
Switch tedeeLockFrontDateChanged "Front door - last change [%s]" <button> (gFrontDoorLock) { channel="mqtt:topic:tedee-front:last_state_change" }
Number tedeeLockFrontBattery "Front door lock battery [%d %%]" <battery> (gFrontDoorLockBattery, gBatteryLevels) ["Status", "LowBattery"] { channel="mqtt:topic:tedee-front:battery" }
Switch tedeeLockFrontBatteryLow "Front door lock Battery low?"      <lowbattery>      (gFrontDoorLockBattery, gBatteryWarnings) ["LowBattery"] 
```

### Map
```
locked=Zamknięty
unlocked=Otwarty
ON=Zamknięty
OFF=Otwarty
=Nieznany
0=Nieskalibrowany
1=Kalibruje się
2=Otwarty
3=Częściowo Zamknięty
4=Otwiera się
5=Zamyka się 
6=Zamknięty
7=Zaciągnięty
8=Zaciąga się
9=Nieznany
18=Aktualizuje się
```

### Sitemap
```
Switch  item=tedeeLockFront icon='door' label='Drzwi frontowe [MAP(tedee.map):%s]'
Text item=tedeeLockFrontBattery
Text item=tedeeLockFrontStatus
Text item=tedeeLockFrontDateChanged
```

## Roadmap
I'm planning to buy a second lock for my other door, so I will extend the flow to support multiple locks.

## Contributing
Feel free to contribute to the flow, I'm happy to accept your PR

## License
Feel fee to use and modify this flow for your needs, it's distributed based on MIT license.

